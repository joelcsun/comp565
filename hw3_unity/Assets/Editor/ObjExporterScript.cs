﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class ObjExporterScript
{
	private static int StartIndex = 0;

	public static void Start()
	{
		StartIndex = 0;
	}
	public static void End()
	{
		StartIndex = 0;
	}

	public static string MaterialsToString(Dictionary<string,Texture> mats){
		StringBuilder sb = new StringBuilder();

		foreach(KeyValuePair<string,Texture> kvp in mats){

			char sep = Path.DirectorySeparatorChar;
			string assetPath = AssetDatabase.GetAssetPath(kvp.Value).Replace("Assets"+sep, "");

			sb.Append(
				"newmtl " + kvp.Key + "\n" +
				"Ns 96.078431\n"+
				"Ka 0.000000 0.000000 0.000000\n"+
				"Kd 0.640000 0.640000 0.640000\n"+
				"Ks 0.500000 0.500000 0.500000\n"+
				"Ni 1.000000\n"+
				"d 1.000000\n"+
				"illum 2\n"+
				"map_Kd " + assetPath + "\n\n"
			);
		}

		return sb.ToString();
	}

	public static string MeshToString(MeshFilter mf, Transform t, Dictionary<string,Texture> exportMats){
		Vector3 s = t.localScale;
		Vector3 p = t.localPosition;
		Quaternion r = t.localRotation;

		int numVertices = 0;
		Mesh m = mf.sharedMesh;
		if (!m)
		{
			return "####Error####";
		}
		Material[] mats = mf.GetComponent<Renderer>().sharedMaterials;

		StringBuilder sb = new StringBuilder();

		foreach (Vector3 vv in m.vertices)
		{
			Vector3 v = t.TransformPoint(vv);
			numVertices++;
			sb.Append(string.Format("v {0} {1} {2}\n", -v.x, v.y, v.z));
		}
		sb.Append("\n");
		foreach (Vector3 nn in m.normals)
		{
			Vector3 v = r * nn;
			sb.Append(string.Format("vn {0} {1} {2}\n", -v.x, v.y, v.z));
		}
		sb.Append("\n");
		foreach (Vector3 v in m.uv)
		{
			sb.Append(string.Format("vt {0} {1}\n", v.x, v.y));
		}
		for (int material = 0; material < m.subMeshCount; material++)
		{
			Material mat = mats[material];
			Texture tex = mat.mainTexture;
			string textureName = (mat.name + "_" + tex.name);
			if(!exportMats.ContainsKey(textureName))
				exportMats.Add(textureName, tex);

			sb.Append("\n");
			sb.Append("usemtl ").Append(textureName).Append("\n");
			sb.Append("usemap ").Append(textureName).Append("\n");

			int[] triangles = m.GetTriangles(material);
			for (int i = 0; i < triangles.Length; i += 3)
			{
				sb.Append(string.Format("f {0}/{0}/{0} {1}/{1}/{1} {2}/{2}/{2}\n",
							triangles[i] + 1 + StartIndex, triangles[i + 1] + 1 + StartIndex, triangles[i + 2] + 1 + StartIndex));
			}
		}

		StartIndex += numVertices;
		return sb.ToString();
	}
}

public class ObjExporter : ScriptableObject
{
	[MenuItem("File/Export/Wavefront OBJ")]
	static void DoExportWSubmeshes()
	{
		DoExport(true);
	}

	[MenuItem("File/Export/Wavefront OBJ (No Submeshes)")]
	static void DoExportWOSubmeshes()
	{
		DoExport(false);
	}

	static void DoExport(bool makeSubmeshes)
	{
		if (Selection.gameObjects.Length == 0)
		{
			Debug.Log("Didn't Export Any Meshes; Nothing was selected!");
			return;
		}

		string meshName = Selection.gameObjects[0].name;
		string fileName = EditorUtility.SaveFilePanel("Export .obj file", "", meshName, "obj");
		string fileNameNoExt = Path.Combine(
			Path.GetDirectoryName(fileName),
			Path.GetFileNameWithoutExtension(fileName)
		);

		ObjExporterScript.Start();

		StringBuilder meshString = new StringBuilder();

		meshString.Append(
				"#" + meshName + ".obj" +
				"\n#" + System.DateTime.Now.ToLongDateString() +
				"\n#" + System.DateTime.Now.ToLongTimeString() +
				"\n#-------" +
				"\nmtllib " + meshName + ".mtl\n"
		);

		Transform t = Selection.gameObjects[0].transform;

		Vector3 originalPosition = t.position;
		t.position = Vector3.zero;

		Dictionary<string,Texture> mats = new Dictionary<string,Texture>();

		if (!makeSubmeshes)
		{
			meshString.Append("g ").Append(t.name).Append("\n");
		}
		meshString.Append(processTransform(t, makeSubmeshes, mats));

		WriteToFile(meshString.ToString(), fileName);
		WriteToFile(ObjExporterScript.MaterialsToString(mats), fileNameNoExt + ".mtl");

		t.position = originalPosition;

		ObjExporterScript.End();
		Debug.Log("Exported Mesh: " + fileName);
	}

	static string processTransform(Transform t, bool makeSubmeshes, Dictionary<string,Texture> mats)
	{
		StringBuilder meshString = new StringBuilder();

		meshString.Append("#" + t.name
				+ "\n#-------"
				+ "\n");

		if (makeSubmeshes)
		{
			meshString.Append("g ").Append(t.name).Append("\n");
		}

		MeshFilter mf = t.GetComponent<MeshFilter>();
		if (mf)
		{
			meshString.Append(ObjExporterScript.MeshToString(mf, t, mats));
		}

		for (int i = 0; i < t.childCount; i++)
		{
			meshString.Append(processTransform(t.GetChild(i), makeSubmeshes, mats));
		}

		return meshString.ToString();
	}

	static void WriteToFile(string s, string filename)
	{
		using (StreamWriter sw = new StreamWriter(filename))
		{
			sw.Write(s);
		}
	}
}
