﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


enum Axis { X, Y, Z }
enum TransformMode { Translate, Scale, Rotate }

interface TransformHandler {
	void Translate(Transform transform, Vector3 vec);
	void Scale    (Transform transform, Vector3 vec);
	void Rotate   (Transform transform, Vector3 vec);
}

class InternalTransformHandler : TransformHandler {
	public void Translate(Transform transform, Vector3 vec){
		transform.Translate(vec);
	}
	public void Scale(Transform transform, Vector3 vec){
		//Use a multiplicative scaling method
		transform.localScale = Vector3.Scale(
			transform.localScale, Vector3.one + vec
		);
	}
	public void Rotate(Transform transform, Vector3 vec){
		transform.Rotate(vec);
	}
}

class CustomTransformHandler : TransformHandler {
	public void Translate(Transform transform, Vector3 vec){
		// Apply this translation transformation matrix to the current matrix
		var matrix = new Matrix4x4(
			new Vector4(1, 0, 0, vec.x),
			new Vector4(0, 1, 0, vec.y),
			new Vector4(0, 0, 1, vec.z),
			new Vector4(0, 0, 0, 1)
		).transpose;
		ApplyTransformationMatrix(transform, matrix);
	}
	public void Scale(Transform transform, Vector3 vec){
		// Apply this scaling transformation matrix to the current matrix
		var matrix = new Matrix4x4(
			new Vector4(1+vec.x, 0, 0, 0),
			new Vector4(0, 1+vec.y, 0, 0),
			new Vector4(0, 0, 1+vec.z, 0),
			new Vector4(0, 0, 0, 1)
		).transpose;
		ApplyTransformationMatrix(transform, matrix);
	}
	public void Rotate(Transform transform, Vector3 vec){
		var matrix = Matrix4x4.identity;
		var vecVal = new float[]{ vec.x, vec.y, vec.z };

		// Loop through each axis and apply the necessary rotation
		for(int axis = 0; axis < vecVal.Length; axis++){

			var cos = Mathf.Cos(vecVal[axis] * Mathf.Deg2Rad);
			var sin = Mathf.Sin(vecVal[axis] * Mathf.Deg2Rad);

			switch(axis){
				case 0: // X-AXIS
					matrix *= new Matrix4x4(
						new Vector4(1, 0, 0, 0),
						new Vector4(0, cos, -sin, 0),
						new Vector4(0, sin, cos, 0),
						new Vector4(0, 0, 0, 1)
					).transpose;
					break;
				case 1: // Y-AXIS
					matrix *= new Matrix4x4(
						new Vector4(cos, 0, sin, 0),
						new Vector4(0, 1, 0, 0),
						new Vector4(-sin, 0, cos, 0),
						new Vector4(0, 0, 0, 1)
					).transpose;
					break;
				case 2: // Z-AXIS
					matrix *= new Matrix4x4(
						new Vector4(cos, -sin, 0, 0),
						new Vector4(sin, cos, 0, 0),
						new Vector4(0, 0, 1, 0),
						new Vector4(0, 0, 0, 1)
					).transpose;
					break;
			}
		}

		ApplyTransformationMatrix(transform, matrix);
	}

	/* Since Unity does not allow us to apply transformation matrices directly
	 * to vertices, we extract the necessary information from the matrix and
	 * apply the updates to the transform object.
	 */
	private void ApplyTransformationMatrix(Transform transform, Matrix4x4 matrix){
		var update = transform.localToWorldMatrix * matrix;
		transform.localPosition = update.GetPosition();
		transform.localScale    = update.GetScale();
		transform.localRotation = update.GetRotation();
	}
}

public class BasicTransformation : MonoBehaviour {

	CustomTransformHandler   customHandler;
	InternalTransformHandler internalHandler;
	TransformHandler currentHandler;
	TransformMode    currentTransformMode;

	void Start(){
		customHandler   = new CustomTransformHandler();
		internalHandler = new InternalTransformHandler();
		currentHandler       = internalHandler;
		currentTransformMode = TransformMode.Translate;
	}

	void UpdateInteractionConfiguration(){
		if(Input.GetKeyDown(KeyCode.M)){
			if(Object.ReferenceEquals(currentHandler, internalHandler)){
				currentHandler = customHandler;
				Debug.Log("Now using CUSTOM transformations.");
			} else {
				currentHandler = internalHandler;
				Debug.Log("Now using INTERNAL transformations.");
			}
		}
		if(Input.GetKeyDown(KeyCode.T))
			currentTransformMode = TransformMode.Translate;
		if(Input.GetKeyDown(KeyCode.S))
			currentTransformMode = TransformMode.Scale;
		if(Input.GetKeyDown(KeyCode.R))
			currentTransformMode = TransformMode.Rotate;
	}

	void HandleInteractions(){
		foreach(Axis axis in System.Enum.GetValues(typeof(Axis))){
			// Verify user is holding down associated axis key
			if(!axis.IsActive())
				continue;

			// Determine the step size. Invert if holding shift. 2 gives nice speed
			var amt = 2 * Time.deltaTime * (Input.GetKey(KeyCode.LeftShift) ? -1 : 1);
			var vec = axis.ToVector() * amt;

			switch(currentTransformMode){
				case TransformMode.Translate:
					currentHandler.Translate(transform, vec);
					break;
				case TransformMode.Scale:
					currentHandler.Scale(transform, vec);
					break;
				case TransformMode.Rotate:
					currentHandler.Rotate(transform, vec * 90);
					break;
			}
		}
	}

	void Update(){
		UpdateInteractionConfiguration();
		HandleInteractions();
	}
}

static class AxisExtensions {
	public static Vector3 ToVector(this Axis axis){
		switch(axis){
			case Axis.X: return Vector3.right;   //1,0,0
			case Axis.Y: return Vector3.up;      //0,1,0
			case Axis.Z: return Vector3.forward; //0,0,1
		}
		return Vector3.zero;
	}
	public static bool IsActive(this Axis axis){
		return (
			(axis == Axis.X && Input.GetKey(KeyCode.X)) ||
			(axis == Axis.Y && Input.GetKey(KeyCode.Y)) ||
			(axis == Axis.Z && Input.GetKey(KeyCode.Z))
		);
	}
}

static class MatrixExtensions {
	public static Vector3 GetScale(this Matrix4x4 matrix){
		return new Vector3(
			matrix.GetColumn(0).magnitude,
			matrix.GetColumn(1).magnitude,
			matrix.GetColumn(2).magnitude
		);
	}
	public static Quaternion GetRotation(this Matrix4x4 matrix){
		return Quaternion.LookRotation(matrix.GetColumn(2), matrix.GetColumn(1));
	}
	public static Vector3 GetPosition(this Matrix4x4 matrix){
		return new Vector3(matrix[0,3], matrix[1,3], matrix[2,3]);
	}
}
