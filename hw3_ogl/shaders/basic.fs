#version 330 core
out vec4 FragColor;

struct Light {
    vec3 position;
    vec3 color;
    float constant;
    float linear;
    float quadratic;
};

in vec3 Normal;
in vec3 FragPos;
in vec2 TexCoords;

uniform sampler2D texture_diffuse1;
uniform vec3 viewPos;
uniform vec3 objectColor;
uniform Light light;

void main()
{    
    // ambient
    float ambientStrength = 0.3;
    vec3 ambient = ambientStrength * light.color;

    // diffuse
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(light.position - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * light.color;

    // specular
    float specularStrength = 0.15;
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 24);
    vec3 specular = specularStrength * spec * light.color;

	float distance    = length(light.position - FragPos);
	float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

    vec3 result = (ambient + diffuse + specular) * attenuation * objectColor;
    FragColor = vec4(result, 1.0);

    FragColor = vec4(result, 1.0) * texture(texture_diffuse1, TexCoords);
}
